def read_json(infile):
    """read class groups from JSON file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import json
    with open(infile, 'r') as f:
        class_groups = json.load(f)

    return class_groups


def read_csv(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import csv
    with open(infile, 'r') as f:
        c = csv.reader(f)
        class_groups = {}
        for row in c:
            class_groups[row[0]] = row[1]

    return class_groups


def read_csv_numpy(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
#    from numpy import loadtxt
    import numpy as np
    with open(infile, 'r') as f:
        recordtype = np.dtype([('name1', np.str_, 20), ('name2', np.str_, 20)])
        txt = np.loadtxt(f, dtype=recordtype, delimiter=',')
        #txt = np.loadtxt(f, dtype=np.str, delimiter=',')
        class_groups = {k : v for k, v in txt }
    # COMPLETE ME!!

    return class_groups

if __name__ == "__main__":
    print(read_csv("class_groups.csv"))
    print(read_csv_numpy("class_groups.csv"))
